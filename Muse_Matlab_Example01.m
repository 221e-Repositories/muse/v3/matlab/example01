%% Clear workspace
clear all
clc

global gyr_sens axl_sens hdr_sens mag_sens
global nSamples axbuff aybuff azbuff s ll

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Discover and connect to Muse device
devlist = blelist;
muse = ble("muse_roberto");

%% Access custom service and characteristics of interest
cmd = characteristic(muse, "C8C0A708-E361-4B5E-A365-98FA6B0A836F", ...
    "D5913036-2D8A-41EE-85B9-4E361AA5C8A7");
dat = characteristic(muse, "C8C0A708-E361-4B5E-A365-98FA6B0A836F", ...
    "09BF2C52-D1D9-C0B7-4145-475964544307");

%% Subscribe to characteristic notify
subscribe(cmd);
subscribe(dat);

%% Get device status (i.e., just to check setup consistency)
% GET_STATE command code (hex): 0x82
write(cmd, [130 0]);    
cmd_response = read(cmd);
state = displayDeviceState(cmd_response);

%% Get device configuration (necessary to manage decoding during stream)
% GET_FULL_SCALES command code (hex): 0xC0
write(cmd, [192 0]);
cmd_response = read(cmd);
[gyr_fs, gyr_sens, axl_fs, axl_sens, hdr_fs, hdr_sens, mag_fs, ...
    mag_sens] = displayDeviceFullScales(cmd_response);

%% Start data acquisition in STREAMING mode
% To stop acquisition at runtime, write the following command on Command
% Window: write(cmd, [2 1 2]). It set the device in SYS_IDLE state.
if (state == 2)
    % SET_STATE command code (hex): 0x02
    % streaming type: 0x08 (continuous streaming)
    % acquisition mode: 0x02 (accelerometer only)
    % acquisition freq: 0x01 (25 Hz)
    write(cmd, [2 5 8 2 0 0 1]);    
else
    disp('Acquisition already running.');
end

%% Manage data acquisition, decoding and visualization
% Setup figure for real time plot
figure; hold all;
nSamples = 1000;
ll = xline(0,'color','k');
buff = nan(nSamples,1);
axbuff = plot(0:nSamples-1,buff,'b');
aybuff = plot(0:nSamples-1,buff,'g');
azbuff = plot(0:nSamples-1,buff,'r');
xlim([0 nSamples]);
s = 0;

% Assign data callback
dat.DataAvailableFcn = @displayCharacteristicData;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% displayDeviceState(arg)
function [state] = displayDeviceState(arg)
    % Check response consistency and positive acknowledge
    if (arg(1) == 0 && arg(3) == 130 && arg(4) == 0)
        % Check buffer at cell index number 5 to get the state code
        switch(arg(5))
            case 2      % SYS_IDLE: 0x02
                state = 2;
                disp('Current state: SYS_IDLE');
            case 4      % SYS_LOG: 0x04
                state = 4;
                disp('Current state: ');
            case 6      % SYS_TX: 0x06 (streaming 128 Byte)
                state = 6;
                disp('Current state: ');
            case 8      % SYS_TX: 0x08 (streaming 20 Byte)
                state = 8;
                disp('Current state: ');
            otherwise
                state = 0;
                disp('Device state not available.');
        end
    else
        disp('Error on retrieving device state!');
    end
end

%% displayDeviceFullScales(arg)
function [gyr_fs, gyr_sens, axl_fs, axl_sens, hdr_fs, hdr_sens, mag_fs, ...
    mag_sens] = displayDeviceFullScales(arg)
    % Check response consistency and positive acknowledge
    if (arg(1) == 0 && arg(3) == 192 && arg(4) == 0)
        % Cast payload to uint16 value
        x = [arg(5) arg(6)];
        y = typecast(uint8(x),'uint16');

        gyr_mask = 3;   % bit mask (hex): 0x0003
        axl_mask = 12;  % bit mask (hex): 0x000c
        hdr_mask = 48;  % bit mask (hex): 0x0030
        mag_mask = 192; % bit mask (hex): 0x00c0

        % Apply bit-mask to get gyr, axl, mag, hdr full scales
        gyr_code = bitand(y,gyr_mask);   
        switch(gyr_code)
            case 0      
                gyr_fs = 245;
                gyr_sens = 0.00875; % dps/LSB
                disp('Gyroscope FS: 245 dps');
            case 1      
                gyr_fs = 500;
                gyr_sens = 0.0175;
                disp('Gyroscope FS: 500 dps');
            case 2      
                gyr_fs = 1000;
                gyr_sens = 0.035;
                disp('Gyroscope FS: 1000 dps');
            case 3      
                gyr_fs = 2000;
                gyr_sens = 0.070;
                disp('Gyroscope FS: 2000 dps');
            otherwise
                gyr_fs = 0;
                gyr_sens = 1;
                disp('Gyroscope FS not available.');
        end

        axl_code = bitand(y,axl_mask);
        switch(axl_code)
            case 0      
                axl_fs = 4;
                axl_sens = 0.122;   % mg/LSB
                disp('Accelerometer FS: 4 g');
            case 8      
                axl_fs = 8;
                axl_sens = 0.244;
                disp('Accelerometer FS: 8 g');
            case 12      
                axl_fs = 16;
                axl_sens = 0.488;
                disp('Accelerometer FS: 16 g');
            case 4      
                axl_fs = 32;
                axl_sens = 0.976;
                disp('Accelerometer FS: 32 g');
            otherwise
                axl_fs = 0;
                axl_sens = 1;
                disp('Accelerometer FS not available.');
        end

        hdr_code = bitand(y,hdr_mask);
        switch(hdr_code)
            case 0      
                hdr_fs = 100;
                hdr_sens = 49;   % mg/LSB
                disp('Accelerometer HDR FS: 100 g');
            case 16      
                hdr_fs = 200;
                hdr_sens = 98;
                disp('Accelerometer HDR FS: 200 g');
            case 48      
                hdr_fs = 400;
                hdr_sens = 195;
                disp('Accelerometer HDR FS: 400 g');
            otherwise
                hdr_fs = 0;
                hdr_sens = 1;
                disp('Accelerometer HDR FS not available.');
        end

        mag_code = bitand(y,mag_mask);
        switch(mag_code)
            case 0      
                mag_fs = 4;
                mag_sens = 0.146156;   % mGauss/LSB
                disp('Magnetometer FS: 4 G');
            case 64      
                mag_fs = 8;
                mag_sens = 0.292312;
                disp('Magnetometer FS: 8 G');
            case 128      
                mag_fs = 12;
                mag_sens = 0.438404;
                disp('Magnetometer FS: 12 G');
            case 192      
                mag_fs = 16;
                mag_sens = 0.584453;
                disp('Magnetometer FS: 16 G');
            otherwise
                mag_fs = 0;
                mag_sens = 1;
                disp('Magnetometer FS not available.');
        end
    else
        disp('Error on retrieving device full scales!');
    end
end

%% Callback function on data characteristic to manage accerometer decoding 
function displayCharacteristicData(src,evt)
    
    global axl_sens
    global axbuff aybuff azbuff s ll

    % Read data
    data = read(src,'oldest');
    s = s + 1;

    % Decode data (in this example we ignore the first 8 bytes that
    % represents the timestamp)
    x = [data(9) data(10)];
    x_val = typecast(uint8(x),'int16');
    x_val = x_val * axl_sens;

    y = [data(11) data(12)];
    y_val = typecast(uint8(y),'int16');
    y_val = y_val * axl_sens;

    z = [data(13) data(14)];
    z_val = typecast(uint8(z),'int16');
    z_val = z_val * axl_sens;

    j = mod(s-1,1000) + 1;
    ll.Value = j;
    axbuff.YData(j) = x_val;
    aybuff.YData(j) = y_val;
    azbuff.YData(j) = z_val;

    % Display and/or plot data
    % Uncomment to display the numeric values
    % fprintf('%f\t%f\t%f\n',x_val,y_val,z_val);
end